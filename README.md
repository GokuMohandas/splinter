# Splinter

### Set up local environment
```
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt
venv/bin/python setup.py develop
```

### Things to keep in mind
- Before using this, play around with the data (class distributions, visualizations, outliers, correlations, etc.)
- Don't try to create a massive API that can handle every ML task. There just too much variability across tasks and models for this to be clean and work well.
- Instead create APIs that can solve one type of task really well or just restrict to one specific task and data.
- What can be shared on data/feature stores, visualization pipelines, etc.

### Airflow instructions
```
http://michal.karzynski.pl/blog/2017/03/19/developing-workflows-with-apache-airflow/
```


find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf

