import os
import pandas as pd

def feature_engineering(df):

    return df

def clean_data(df, drop_columns):

    # Drop columns
    df = df.drop(drop_columns, axis=1)

    # Drop rows with any nan values
    df = df.dropna(axis=0, how='any')

    return df

def preprocess_data(df, drop_columns):

    # Feature engineering
    df = feature_engineering(df)

    # Cleaning
    df = clean_data(df, drop_columns)

    return df