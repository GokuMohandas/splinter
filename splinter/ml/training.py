import os
import json
import numpy as np
import time
from tqdm import tqdm, trange
import torch
from torch import nn, FloatTensor, DoubleTensor
from torch.nn import MSELoss, CrossEntropyLoss
from torch.optim import Adam, SGD, lr_scheduler
from torch.utils.data import DataLoader

from splinter.ml.split import split_data
from splinter.ml.preprocess import preprocess_data
from splinter.ml.dataset import Dataset
from splinter.ml.models import MLP


def make_training_state(args):
    return {'stop_early': False,
            'early_stopping_step': 0,
            'early_stopping_best_val': 1e8,
            'learning_rate': args.LEARNING_RATE,
            'epoch_index': 0,
            'train_loss': [],
            'train_acc': [],
            'val_loss': [],
            'val_acc': [],
            'test_loss': -1,
            'test_acc': -1,
            'model_filename': args.MODEL_FILE,
            'task': args.TASK,
            'creation_time': args.TIMESTAMP,
            'done_training': False
            }

def update_training_state(args, model, training_state):
    """Handle the training state updates.
    """

    # Save one model at least
    if training_state['epoch_index'] == 0:
        torch.save(model.state_dict(), training_state['model_filename'])
        training_state['stop_early'] = False

    # Save model if performance improved
    elif training_state['epoch_index'] >= 1:
        loss_t = training_state['val_loss'][-1]

        # Loss worsened
        if loss_t >= training_state['early_stopping_best_val']:
            training_state['early_stopping_step'] += 1

        # Loss improved
        else:
            if loss_t < training_state['early_stopping_best_val']:
                torch.save(model.state_dict(), training_state['model_filename'])
                training_state['early_stopping_best_val'] = loss_t

                # Reset early stopping step
                training_state['early_stopping_step'] = 0

        # Stop early ?
        training_state['stop_early'] = \
            training_state['early_stopping_step'] >= args.EARLY_STOPPING_CRITERIA

    return training_state

def update_training_details(training_state, training_details_file):

    # Load previosly saved details (prior to training)
    with open(training_details_file, "r") as f:
        training_details = json.load(f)

    # Update training details
    for key, val in training_state.items():
        training_details[key] = val

    # Update training details file
    with open(training_details_file, "w") as f:
        json.dump(training_details, f)

def compute_accuracy(y_pred, y_target):
    y_target = y_target.cpu()
    y_pred_indices = y_pred.cpu().max(dim=1)[1]
    n_correct = torch.eq(y_pred_indices, y_target).sum()
    return (float(n_correct) / len(y_pred_indices)) * 100.

def generate_batches(dataset, batch_size, shuffle=False,
                     drop_last=False, use_cuda=False):

    dataloader = DataLoader(dataset=dataset, batch_size=batch_size,
                            shuffle=shuffle, drop_last=drop_last)

    for data_dict in dataloader: # uses dataset.__getitem__()
        out_data_dict = {}
        for name, tensor in data_dict.items():
            if name == 'y_target':
                out_data_dict[name] = tensor[0]
            else:
                out_data_dict[name] = torch.stack(tensor, dim=1).view(-1, len(tensor))
            if use_cuda:
                out_data_dict[name] = out_data_dict[name].cuda()
        yield out_data_dict

def run_training_loop(args, dataset, vectorizer):
    """
    """

    # Initialize model
    in_features = len(vectorizer.X_vocabulary)
    model = MLP(in_features=in_features,
                num_hidden_units=args.NUM_HIDDEN_UNITS,
                out_features=args.NUM_CLASSES,
                dropout_p=args.DROPOUT_P)
    if args.CUDA:
        model = model.cuda()
    else:
        model = model.cpu()

    # Loss, optimizer and scheduler
    loss_func = CrossEntropyLoss(dataset.class_weights)
    optimizer = Adam(model.parameters(), lr=args.LEARNING_RATE)
    scheduler = lr_scheduler.ReduceLROnPlateau(
        optimizer=optimizer, mode='min', factor=0.5, patience=1)

    # Prepare train state
    training_state = make_training_state(args)

    # Process batches
    for epoch_index in tqdm(range(args.NUM_EPOCHS)):
        training_state['epoch_index'] = epoch_index

        # Iterate over training dataset

        # setup: batch generator, set loss and dev to 0, set train mode on
        dataset.set_split('train')
        batch_generator = generate_batches(
            dataset, batch_size=args.BATCH_SIZE, shuffle=False,
            drop_last=False, use_cuda=args.CUDA)

        running_loss = 0.0
        running_acc = 0.0
        model.train()

        for batch_index, batch_dict in enumerate(batch_generator):
            # the training routine is these 5 steps:

            # --------------------------------------
            # step 1. zero the gradients
            optimizer.zero_grad()

            # step 2. compute the output
            y_pred = model(x_in=batch_dict['x_data'].float())

            # step 3. compute the loss
            loss = loss_func(y_pred, batch_dict['y_target'])
            loss_t = loss.cpu()
            running_loss += (loss_t - running_loss) / (batch_index + 1)

            # step 4. use loss to produce gradients
            loss.backward()

            # step 5. use optimizer to take gradient step
            optimizer.step()
            # -----------------------------------------

            # Compute the accuracy
            acc_t = compute_accuracy(y_pred, batch_dict['y_target'])
            running_acc += (acc_t - running_acc) / (batch_index + 1)

        training_state['train_loss'].append(round(running_loss.item(), 3))
        training_state['train_acc'].append(round(running_acc, 3))

        # Iterate over val dataset

        # setup: batch generator, set loss and dev to 0; set eval mode on
        dataset.set_split('val')
        batch_generator = generate_batches(
            dataset, batch_size=args.BATCH_SIZE, shuffle=False,
            drop_last=False, use_cuda=args.CUDA)

        running_loss = 0.0
        running_acc = 0.0
        model.eval()

        for batch_index, batch_dict in enumerate(batch_generator):

            # compute the output
            y_pred = model(x_in=batch_dict['x_data'].float())

            # step 3. compute the loss
            loss = loss_func(y_pred, batch_dict['y_target'])
            loss_t = loss.cpu()
            running_loss += (loss_t - running_loss) / (batch_index + 1)

            # Compute the accuracy
            acc_t = compute_accuracy(y_pred, batch_dict['y_target'])
            running_acc += (acc_t - running_acc) / (batch_index + 1)

        training_state['val_loss'].append(round(running_loss.item(), 3))
        training_state['val_acc'].append(round(running_acc, 3))

        training_state = update_training_state(args=args, model=model,
            training_state=training_state)

        scheduler.step(training_state['val_loss'][-1])

        if args.VERBOSE:
            print ("EPOCH: {0}, LR: {1}, TRAIN_LOSS: {2}, TRAIN_ACC: {3}, VAL_LOSS: {4}, VAL_ACC: {5}".format(
                training_state['epoch_index'], training_state["learning_rate"],
                training_state["train_loss"][-1], training_state["train_acc"][-1],
                training_state["val_loss"][-1], training_state["val_acc"][-1]))

        # Stop early
        if training_state['stop_early']:
            training_state['done_training'] = True
            update_training_details(training_state=training_state,
                training_details_file=args.TRAINING_DETAILS_FILE)
            break

        # Update train details json
        update_training_details(training_state=training_state,
                training_details_file=args.TRAINING_DETAILS_FILE)

    # Test loss and deviance
    model.load_state_dict(torch.load(training_state['model_filename']))
    if args.CUDA:
        model = model.cuda()
    else:
        model = model.cpu()

    dataset.set_split('test')
    batch_generator = generate_batches(dataset, batch_size=args.BATCH_SIZE,
        shuffle=False, drop_last=False, use_cuda=args.CUDA)

    running_loss = 0.0
    running_acc = 0.0
    model.eval()
    for batch_index, batch_dict in enumerate(batch_generator):
        # compute the output
        y_pred = model(x_in=batch_dict['x_data'].float())

        # compute the loss
        loss = loss_func(y_pred, batch_dict['y_target'])
        loss_t = loss.cpu()
        running_loss += (loss_t - running_loss) / (batch_index + 1)

        # Compute the accuracy
        acc_t = compute_accuracy(y_pred, batch_dict['y_target'])
        running_acc += (acc_t - running_acc) / (batch_index + 1)

    training_state['test_loss'] = round(running_loss.item(), 3)
    training_state['test_acc'] = round(running_acc, 3)

    # Update training_state
    training_state['done_training'] = True
    update_training_details(training_state=training_state,
                training_details_file=args.TRAINING_DETAILS_FILE)

    return training_state


def train_model(args, reload_from_files, processed_data_file, vectorizer_file):

    # Load dataset
    if reload_from_files:
        # Reload dataset and vectorizer from a checkpoint
        dataset = Dataset.load_dataset_and_load_vectorizer(processed_data_file,
            args.TARGET_METRIC, args.FEATURES, vectorizer_file)
    else:
        # Loading dataset and creating vectorizer
        dataset = Dataset.load_dataset_and_make_vectorizer(processed_data_file,
            args.TARGET_METRIC, args.FEATURES)
        dataset.save_vectorizer(vectorizer_file)

    # Load vectorizer
    vectorizer = dataset.get_vectorizer()

    # Run training loop
    training_state = run_training_loop(args=args, dataset=dataset,
        vectorizer=vectorizer)


def train_operations(training_config):

    # Split the data
    df = split_data(raw_data_file=training_config.RAW_DATA_FILE,
        train_proportion=training_config.TRAIN_PROPORTION,
        val_proportion=training_config.VAL_PROPORTION,
        test_proportion=training_config.TEST_PROPORTION,
        shuffle=training_config.SHUFFLE)

    # Preprocess the data
    df = preprocess_data(df=df, drop_columns=training_config.DROP_COLUMNS)

    # Save dataframe to CSV
    df.to_csv(training_config.PROCESSED_DATA_FILE, index=False)

    # Train model
    train_model(args=training_config,
        reload_from_files=training_config.RELOAD_FROM_FILES,
        processed_data_file=training_config.PROCESSED_DATA_FILE,
        vectorizer_file=training_config.VECTORIZER_FILE)

