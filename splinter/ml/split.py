import os
import pandas as pd

def split_data(raw_data_file, train_proportion, val_proportion, test_proportion,
    shuffle):

    # Read raw data
    df = pd.read_csv(raw_data_file, header=0)

    # Shuffling
    if shuffle:
        df = df.sample(frac=1).reset_index(drop=True)

    # Get lengths
    n = len(df)
    n_train = int(train_proportion*n)
    n_val = int(val_proportion*n)
    n_test = int(test_proportion*n)

    def get_split(row):
        index = row.name
        if index < n_train:
            return 'train'
        if n_train <= index < n_train+n_val:
            return 'val'
        else:
            return 'test'

    # Apply splits
    df['split'] = df.apply(get_split, axis=1)

    return df


