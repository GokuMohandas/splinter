import os
import json
import numpy as np
import pandas as pd
import string
import torch
from torch import FloatTensor
from torch.utils.data import DataLoader

from splinter.ml.vocabulary import X_Vocabulary, y_Vocabulary

class Dataset(object):
    """Class to hold the
    processed data."""

    def __init__(self, df, target_metric, features, vectorizer):

        self.df = df
        self._vectorizer = vectorizer
        self.target_metric = target_metric
        self.features = features

        self.train_df = self.df[self.df.split=="train"]
        self.train_size = len(self.train_df)

        self.val_df = self.df[self.df.split=="val"]
        self.validation_size = len(self.val_df)

        self.test_df = self.df[self.df.split=="test"]
        self.test_size = len(self.test_df)

        self.infer_df = self.df[self.df.split=="infer"]
        self.infer_size = len(self.infer_df)

        self._lookup_dict = {"train": (self.train_df, self.train_size),
                             "val": (self.val_df, self.validation_size),
                             "test": (self.test_df, self.test_size),
                             "infer": (self.infer_df, self.infer_size)}

        self.set_split("train")

        # Class weights (useful when we have uneven class distributions)
        class_counts = self.train_df[target_metric].value_counts().to_dict()
        sorted_counts = sorted(
            class_counts.items(),
            key=lambda item: \
                self._vectorizer.y_vocabulary.lookup_token(item[0]))
        self.class_weights = 1.0 / FloatTensor(
            [float(count) for _, count in sorted_counts])

    @classmethod
    def load_dataset_and_make_vectorizer(cls, data_csv, target_metric, features):
        df = pd.read_csv(data_csv)
        train_df = df[df.split=="train"]
        return cls(df, target_metric, features,
            Vectorizer.from_dataframe(train_df, target_metric))

    @classmethod
    def load_dataset_and_load_vectorizer(cls, data_csv, target_metric,
        features, vectorizer_filepath):
        df = pd.read_csv(data_csv)
        vectorizer = cls.load_vectorizer_only(vectorizer_filepath)
        return cls(df, target_metric, features, vectorizer)

    @staticmethod
    def load_vectorizer_only(vectorizer_filepath):
        with open(vectorizer_filepath) as fp:
            return Vectorizer.from_serializable(json.load(fp))

    def save_vectorizer(self, vectorizer_filepath):
        with open(vectorizer_filepath, "w") as fp:
            json.dump(self._vectorizer.to_serializable(), fp)

    def get_vectorizer(self):
        return self._vectorizer

    def set_split(self, split="train"):
        self._target_split = split
        self._target_df, self._target_size = self._lookup_dict[split]

    def __len__(self):
        return self._target_size

    def __getitem__(self, index):

        # Get row (drop split column)
        row = self._target_df.iloc[index]
        row = row.drop(["split"], axis=0)

        # Features
        X_features = self.features
        y_features = [self.target_metric]

        # Values
        X_values = {feature: row[feature] for feature in X_features}
        y_values = {feature: row[feature] for feature in y_features}

        # Vectorize
        X = self._vectorizer.X_vectorize(X_features, X_values)
        y = self._vectorizer.y_vectorize(y_features, y_values)

        return {"x_data": X,
                "y_target": y}

    def get_num_batches(self, batch_size):
        return len(self) // batch_size

class Vectorizer(object):
    """Class to apply transformations
    on the data."""

    def __init__(self, X_vocabulary, y_vocabulary):
        self.X_vocabulary = X_vocabulary
        self.y_vocabulary = y_vocabulary

    def X_vectorize(self, features, values):
        vectorized_values = []
        for feature in features:
            mean = self.X_vocabulary.lookup_feature_mean(feature)
            std = self.X_vocabulary.lookup_feature_std(feature)
            vectorized_value = (values[feature] - mean) / std
            vectorized_values.append(vectorized_value)
        return vectorized_values

    def y_vectorize(self, features, values):
        vectorized_values = []
        for feature in features:
            vectorized_value = self.y_vocabulary.lookup_token(values[feature])
            vectorized_values.append(vectorized_value)
        return vectorized_values


    def unstandardize(self, feature, values):
        """Unstandardize a single
        feature's values."""
        mean = self.vocabulary.lookup_feature_mean(feature)
        std = self.vocabulary.lookup_feature_std(feature)
        unvectorized_values = np.abs((values * std) + mean)
        return unvectorized_values

    @classmethod
    def from_dataframe(cls, df, target_metric):
        X_vocabulary = X_Vocabulary()
        y_vocabulary = y_Vocabulary()

        # Get features (remove split)
        features = list(df.columns.values)
        features.remove("split")
        features.remove(target_metric)

        # Process each feature (train values only)
        for feature in features:
            values = df.loc[df.split == "train"][feature]
            X_vocabulary.process_feature(feature, values)

        # Target metric's vocabulary
        for _class in df[target_metric].unique():
            y_vocabulary.add_token(_class)

        return cls(X_vocabulary, y_vocabulary)

    @classmethod
    def from_serializable(cls, contents):
        X_vocabulary = X_Vocabulary.from_serializable(contents['X_vocabulary'])
        y_vocabulary =  y_Vocabulary.from_serializable(contents['y_vocabulary'])
        return cls(X_vocabulary=X_vocabulary, y_vocabulary=y_vocabulary)

    def to_serializable(self):
        return {'X_vocabulary': self.X_vocabulary.to_serializable(),
                'y_vocabulary': self.y_vocabulary.to_serializable()}
