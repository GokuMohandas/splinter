import os
import json
import numpy as np
import pandas as pd
import time
from tqdm import tqdm, trange
import torch
from torch import nn, FloatTensor, DoubleTensor
from torch.nn import MSELoss, CrossEntropyLoss
from torch.optim import Adam, SGD, lr_scheduler
from torch.utils.data import DataLoader

from splinter.ml.dataset import Vectorizer
from splinter.ml.preprocess import preprocess_data
from splinter.ml.dataset import Dataset
from splinter.ml.models import MLP
from splinter.ml.training import generate_batches


def initialize_vectorizer_and_model(training_config, inference_config):

    # Get vectorizer
    with open(inference_config.VECTORIZER_FILE) as fp:
        vectorizer = Vectorizer.from_serializable(json.load(fp))

    # Initialize model
    in_features = len(vectorizer.X_vocabulary)
    model = MLP(in_features=in_features,
                num_hidden_units=training_config.NUM_HIDDEN_UNITS,
                out_features=training_config.NUM_CLASSES,
                dropout_p=training_config.DROPOUT_P)
    model.load_state_dict(torch.load(inference_config.MODEL_FILE))
    if inference_config.CUDA:
        model = model.cuda()
    else:
        model = model.cpu()

    return vectorizer, model


def infer(training_config, inference_config):

    # Load vectorizer and saved model
    vectorizer, model = initialize_vectorizer_and_model(
        training_config=training_config,
        inference_config=inference_config)

    # Read data needed for inference
    df_inference = pd.read_csv(inference_config.ORIGIN, header=0)

    # Preprocess data
    df = preprocess_data(df=df_inference,
        drop_columns=training_config.DROP_COLUMNS)

    # Add split column
    df["split"] = "infer"

    # Add empty y column
    df.insert(loc=0, column=training_config.TARGET_METRIC, value=None)

    # Create inference Dataset instance
    dataset = Dataset(df=df,
        target_metric=training_config.TARGET_METRIC,
        features=training_config.FEATURES,
        vectorizer=vectorizer)

    # setup: batch generator, set loss and dev to 0, set train mode on
    dataset.set_split('infer')
    batch_generator = generate_batches(
        dataset, batch_size=training_config.BATCH_SIZE, shuffle=False,
        drop_last=False, use_cuda=inference_config.CUDA)

    # Set to evaluation mode (needed for dropout layers, etc.)
    model.eval()

    # Get predictions
    for batch_index, batch_dict in enumerate(batch_generator):
        y_pred = model(x_in=batch_dict['x_data'].float(), apply_softmax=True)

    # Get list of classes in order of idx
    classes = [key for key in [vectorizer.y_vocabulary._idx_to_token[idx] \
        for idx in range(len(vectorizer.y_vocabulary._idx_to_token))]]

    # Save predictions to df_inference
    for i, _class in enumerate(classes):
        df_inference[_class] = y_pred.detach().numpy()[:, i]

    # Save to csv
    df_inference.to_csv(inference_config.INFERENCE_FILE, index=False)

    return



