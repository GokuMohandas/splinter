import os
import torch
from torch import FloatTensor
from torch.nn import Dropout, Linear, Module, ReLU, init, functional as F
from torch.nn.init import calculate_gain
from torch.nn.parameter import Parameter

class MLP(Module):
    """
    Multi-layer perceptron,
    """
    def __init__(self, in_features, num_hidden_units, out_features,
                 dropout_p):
        super(MLP, self).__init__()
        self.fc1 = Linear(in_features, num_hidden_units)
        self.fc2 = Linear(num_hidden_units, num_hidden_units)
        self.fc3 = Linear(num_hidden_units, out_features)
        self.relu = ReLU()
        self.dropout = Dropout(dropout_p)

        # Xavier initialization
        self.init_weights()

    def init_weights(self):

        # Weights
        init.xavier_uniform_(self.fc1.weight, calculate_gain("relu"))
        init.xavier_uniform_(self.fc2.weight, calculate_gain("relu"))

        # Even with detaching, this doesn't work...just commenting for now
#         # Biases
#         self.fc1.bias.detach().normal_(mean=0, std=0.1)
#         self.fc2.bias.detach().normal_(mean=0, std=0.1)
#         self.fc3.bias.detach().normal_(mean=0, std=0.1)

    def forward(self, x_in, apply_softmax=False):

        z = self.relu(self.fc1(x_in))
        z = self.dropout(z)
        z = self.relu(self.fc2(z))
        z = self.dropout(z)
        y_out = self.fc3(z)

        # optionally apply the softmax
        if apply_softmax:
            y_out = F.softmax(y_out, dim=1)

        return y_out
