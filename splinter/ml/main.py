import os

from splinter.utils import handle_dirs
from splinter.ml.config import Config, SplitConfig, PreprocessConfig, TrainConfig
from splinter.ml.split import split_data
from splinter.ml.preprocess import preprocess_data
from splinter.ml.train import train_model
from splinter.ml.utils import set_seed_everywhere

def train():

    # Set seed for reproducability
    set_seed_everywhere(seed=Config.SEED, cuda=Config.CUDA)

    # Create model storage
    handle_dirs(Config.MODEL_DIR)

    # Split the data
    df = split_data(raw_data_file=SplitConfig.RAW_DATA_FILE,
        train_proportion=SplitConfig.TRAIN_PROPORTION,
        val_proportion=SplitConfig.VAL_PROPORTION,
        test_proportion=SplitConfig.TEST_PROPORTION,
        shuffle=SplitConfig.SHUFFLE)

    # Preprocess the data
    df = preprocess_data(df=df, drop_columns=PreprocessConfig.DROP_COLUMNS,
        processed_data_file=Config.PROCESSED_DATA_FILE)

    # Train model
    train_model(reload_from_files=TrainConfig.RELOAD_FROM_FILES,
        processed_data_file=Config.PROCESSED_DATA_FILE,
        vectorizer_file=TrainConfig.VECTORIZER_FILE)
