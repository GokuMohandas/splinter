import os
import json
import numpy as np
import pandas as pd
import string
import torch
from torch import FloatTensor
from torch.utils.data import DataLoader

class X_Vocabulary(object):
    """Class that will hold the
    train mean and std for
    each feature."""

    def __init__(self, feature_stats=None):

        if feature_stats is None:
            feature_stats = {}

        self._feature_stats = feature_stats

    def to_serializable(self):
        return {'feature_stats': self._feature_stats}

    @classmethod
    def from_serializable(cls, contents):
        return cls(**contents)

    def process_feature(self, feature, values):
        """Process the train mean and
        std for the feature."""

        self._feature_stats[feature] = {}
        self._feature_stats[feature]['mean'] = np.mean(values)
        self._feature_stats[feature]['std'] = np.std(values)

    def lookup_feature_mean(self, feature):
        if feature not in self._feature_stats:
            raise KeyError("Feature {} not in vocabulary.".format(feature))
        mean = self._feature_stats[feature]['mean']
        return mean

    def lookup_feature_std(self, feature):
        if feature not in self._feature_stats:
            raise KeyError("Feature {} not in vocabulary.".format(feature))
        std = self._feature_stats[feature]['std']
        return std

    def __str__(self):
        return "<X_Vocabulary(size=%d)>" % len(self)

    def __repr__(self):
        return str(self)

    def __len__(self):
        return len(self._feature_stats)

class y_Vocabulary(object):
    """ Vocab for the flowers.
    """

    def __init__(self, token_to_idx=None):

        if token_to_idx is None:
            token_to_idx = {}
        self._token_to_idx = token_to_idx

        # idx --> token
        self._idx_to_token = {idx: token
                              for token, idx in self._token_to_idx.items()}

        self.unk_index = -1

    def add_unk_token(self, unk_token):
        self.unk_index = self.add_token(unk_token)

    def to_serializable(self):
        return {'token_to_idx': self._token_to_idx}

    @classmethod
    def from_serializable(cls, contents):
        return cls(**contents)

    def add_token(self, token):
        try:
            index = self._token_to_idx[token]
        except KeyError:
            index = len(self._token_to_idx)
            self._token_to_idx[token] = index
            self._idx_to_token[index] = token
        return index

    def lookup_token(self, token):
        if self.unk_index >= 0:
            return self._token_to_idx.get(token, self.unk_index)
        else:
            if token is None: # inference
                return 0
            return self._token_to_idx[token]

    def lookup_index(self, index):

        print (self._token_to_idx)
        if index not in self._idx_to_token:
            raise KeyError("the index {0} is not in the Vocabulary".format(index))
        return self._idx_to_token[index]

    def __str__(self):
        return "<y_Vocabulary(size=%d)>" % len(self)

    def __repr__(self):
        return str(self)

    def __len__(self):
        return len(self._token_to_idx)
