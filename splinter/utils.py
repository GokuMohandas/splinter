import os
import datetime
from flask import flash, redirect, session, url_for
from functools import wraps
import logging
from logging.handlers import RotatingFileHandler
import time

# Logging
_format = '%(asctime)s:%(filename)s:%(funcName)s:%(lineno)d:%(levelname)s:%(message)s'
formatter = logging.Formatter(_format)

def setup_logger(name, log_file, level=logging.DEBUG):
    """Function setup as many loggers as you want"""

    handler = RotatingFileHandler(log_file, maxBytes=1024*1024*1, backupCount=1)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

def handle_dirs(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
