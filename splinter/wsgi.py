import os

from splinter.application import application
from splinter.config import TaskConfig, DataConfig, TrainingConfig, InferenceConfig
from splinter.utils import handle_dirs

if __name__ == "__main__":

    # Configurations and make tasks dir (if it doesn't exist)
    task_config = TaskConfig()
    handle_dirs(task_config.TASKS_DIR)

    application.run(host=application.config['HOST'], port=application.config['PORT'])
