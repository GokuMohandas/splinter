// Disable a anchor tag or button after it is clicked
function clickAndDisable(link) {
     // disable subsequent clicks
     link.onclick = function(event) {
        event.preventDefault();
     }
   }


// Show spinner until next page loads
function loadSpinner() {
    setTimeout(function() {
        $('#loading-spinner').show();
        $('#loading-page').show();
    }, 1000) // wait 1 second before showing loading spinner
}

// Bootstrap 4 modal not working with dropdown menu
$('.dropdown-menu').click(function(e) {
    e.stopPropagation();
    if ($(e.target).is('[data-toggle=modal]')) {
        $($(e.target).data('target')).modal()
    }
});

// Initialize tooltip component
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// // Updates tooltip
// $(document).ready(function() {
//     setTimeout(function(){ $('a.updates-tooltip').tooltip('hide'); }, 3000);
//     setTimeout(function(){ $('a.updates-tooltip').tooltip('show'); }, 3000);
//     setTimeout(function(){ $('a.updates-tooltip').tooltip('hide'); }, 8000);
// });

// Initialize popover component
$(function () {
  $('[data-toggle="popover"]').popover()
})

// Side bar
$('.collapse.in').collapse({
  toggle: true
})
