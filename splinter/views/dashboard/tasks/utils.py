import os
from datetime import datetime
import json
import pandas as pd
import shutil
from threading import Thread
import time
import uuid

from splinter.config import TaskConfig
from splinter.utils import handle_dirs

def get_tasks():

    # Configurations
    task_config = TaskConfig()

    # Get tasks
    tasks = [f for f in os.listdir(task_config.TASKS_DIR) \
        if os.path.isdir(os.path.join(task_config.TASKS_DIR, f))]

    return tasks

def get_task_details(task):

    # Configurations
    task_config = TaskConfig(task=task)

    # Get data details
    with open(task_config.TASK_DETAILS_FILE, "r") as f:
        task_details = json.load(f)

    return task_details

def create_task(config):

    # Check is task name is already taken
    tasks = get_tasks()
    if task in tasks:
        return False

    # Configurations
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    task_config = TaskConfig(task=config["task"])

    # Create task dir
    handle_dirs(task_config.TASKS_DIR)
    handle_dirs(task_config.TASK_DIR)
    handle_dirs(task_config.DATAS_DIR)
    handle_dirs(task_config.MODELS_DIR)

    # Data details
    task_details = {
        "task": config["task"],
        "description": config["description"],
        "tags": config["tags"],
        "creation_time": timestamp
    }

    # Save data details
    with open(task_config.TASK_DETAILS_FILE, "w") as f:
        json.dump(task_details, f)

    return task_details

def delete_task(task):

    # Configurations
    task_config = TaskConfig(task=task)

    # Delete the dir recursively if it exists
    shutil.rmtree(task_config.TASK_DIR)

    # Update response
    return "Successfully deleted task {0}".format(task)

