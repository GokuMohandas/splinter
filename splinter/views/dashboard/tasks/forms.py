import os
from flask import Blueprint, flash, Markup, redirect, render_template, request, \
                  session, url_for
from functools import wraps
from passlib.hash import sha256_crypt
import time
from wtforms import Form, BooleanField, FileField, PasswordField, StringField, TextAreaField, validators
from wtforms.fields.html5 import EmailField

class NewTaskForm(Form):
    task = StringField("task", [
        validators.DataRequired(message="Task name is required"),
        validators.Length(min=1, max=100, message="Not a valid task name")])
    description = StringField("description")
    tags = StringField('tags')
