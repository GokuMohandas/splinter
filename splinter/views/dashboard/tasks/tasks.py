import os
import json
from flask import Blueprint, flash, Markup, redirect, render_template, request, \
    session, url_for
import random
import requests

from splinter.config import BASE_DIR, ml_logger, cache, version
from splinter.views.dashboard.tasks.forms import NewTaskForm
from splinter.views.dashboard.tasks.utils import get_tasks, get_task_details, \
    create_task, delete_task

_tasks = Blueprint("_tasks", __name__)

# Index
@_tasks.route("/", methods=["GET"])
#@cache.cached(timeout=3600)
def tasks():
    """
    """

    # Get list of tasks
    tasks = get_tasks()

    # Get task details
    task_details = {}
    for task in tasks:
        task_details[task] = get_task_details(task=task)

    return render_template("dashboard/tasks/tasks.html", enumerate=enumerate, tasks=tasks,
        task_details=task_details)

# Task details
@_tasks.route("/task_details/<task>/", methods=["GET"])
#@cache.cached(timeout=3600)
def task_details(task):
    """
    """

    # Get data details
    task_details = get_task_details(task=task)

    return render_template("dashboard/tasks/task_details.html",
        task_details=task_details)

# New task
@_tasks.route("/new_task/", methods=['GET', 'POST'])
#@cache.cached(timeout=3600)
def new_task():
    """
    """

    form = NewTaskForm(request.form)

    if request.method == 'GET':
        return render_template('dashboard/tasks/new_task.html')

    elif request.method == 'POST':

        # Get intent if it exists
        intent = request.form['submit']

        # Cancel
        if intent == "cancel":
            return redirect(url_for('_tasks.tasks'))

        if form.validate():

            if intent == "create":

                # Get form inputs
                config = {}
                config["task"] = form.task.data
                config["description"] = form.description.data
                config["tags"] = [tag.strip() for tag in form.tags.data.split(",")]

                # Create task
                task_details = create_task(config=config)

                if not task_details:
                    flash("Task {0} already exists!".format(task), "danger")
                    return redirect(url_for('_tasks.new_task'))

                # Flash message
                flash("New task {0} successfully created!".format(task), "success")
                return redirect(url_for('_tasks.tasks'))

            else:
                flash("Something went wrong, please try again", 'danger')
                return redirect(url_for('_tasks.tasks'))

        elif not form.validate():

            all_errors = []
            for error in form.errors:
                all_errors.append(form.errors[error][0])
            flash(', '.join([error for error in all_errors]), 'danger')
            return redirect(url_for('_tasks.new_task'))

# Delete a task
@_tasks.route("/delete_task/<task>/", methods=["GET"])
#@cache.cached(timeout=3600)
def remove_task(task):
    """
    """

    # Get data details
    delete_task(task=task)

    flash("Task {0} was successfully deleted!".format(task), "success")
    return redirect(url_for('_tasks.tasks'))

