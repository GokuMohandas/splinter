import os
import json
from flask import Blueprint, jsonify, render_template, request
import random
import requests

from splinter.config import BASE_DIR, ml_logger, cache, version

_api = Blueprint("_api", __name__)

# Health check
@_api.route("/api/", methods=["GET"])
#@cache.cached(timeout=3600)
def _health_check():
    """Index main page.
    """
    return jsonify({"status": "Splinter is live!"})

# Get tasks
@_api.route("/api/tasks/", methods=["GET"])
def _tasks():
    """Get all tasks.
    """

    # Get tasks
    tasks = get_tasks()

    return jsonify({"tasks": tasks})


# Get model ids
@_api.route("/api/data/<task>", methods=["GET"])
def _data(task):
    """Get data versions for a task.
    """

    # Get data versions
    data_versions = get_data_versions(task=task)

    return jsonify({"data_versions": data_versions})

# Get model ids
@_api.route("/api/models/<task>", methods=["GET"])
def _models(task):
    """Get all available models for a task.
    """

    # Get model versions
    model_versions = get_model_versions(task=task)

    return jsonify({"model versions": model_versions})

# Create task
@_api.route("/api/create_task/", methods=["POST"])
def _create_task():
    """Training given a task and data.
    """

    if request.method == "POST":

        # Extract the data
        task = request.json["task"]
        description = request.json["description"]
        tags = request.json["tags"]

        # Create the task
        task_details = create_task(config_file=config_file)

        return jsonify({"task_details": task_details})

# Task details for a task
@_api.route("/api/task_details/<task>/", methods=["GET"])
def _task_details(task):

    # Get task details
    task_details = get_task_details(task=task)

    return jsonify({"task_details": task_details})

# Post training data
@_api.route("/api/post_data/", methods=["POST"])
def _post_data():
    """Training given a task and data.
    """

    if request.method == "POST":

        # Extract the data
        config_file = request.json["config_file"]

        # Post the data
        data_details = post_data(config_file=config_file)

        return jsonify({"data_details": data_details})

# Data details for an ID
@_api.route("/api/data_details/<task>/<data_version>/", methods=["GET"])
def _data_details(task, data_version):

    # Get data details
    data_details = get_data_details(task=task, data_version=data_version)

    return jsonify({"data_details": data_details})

# Training a model
@_api.route("/api/train_model/", methods=["POST"])
def _train_model():
    """Training.
    """

    if request.method == "POST":

        # Extract the data
        config_file = request.json["config_file"]

        # Training
        training_details = train(config_file=config_file)

        return jsonify({"training_details": training_details})

# training details for an ID
@_api.route("/api/training_details/<task>/<model_version>/", methods=["GET"])
def _training_details(task, model_version):

    training_details = get_training_details(task=task,
        model_version=model_version)

    return jsonify({"training_details": training_details})

# Delete the task
@_api.route("/api/delete_task/<task>/", methods=["GET"])
def _delete_task(task):

    # Delete the task
    response = delete_task(task=task)

    return jsonify({"response": response})

# Delete data version
@_api.route("/api/delete_data/<task>/<data_version>/", methods=["GET"])
def _delete_data(task, data_version):

    # Delete the data
    response = delete_data(task=task, data_version=data_version)

    return jsonify({"response": response})

# Delete model version
@_api.route("/api/delete_model/<task>/<model_version>/", methods=["GET"])
def _delete_model(task, model_version):

    # Delete the model
    response = delete_model(task=task, model_version=model_version)

    return jsonify({"response": response})

# Predictions
@_api.route("/api/infer/", methods=["POST"])
def _inference():

    if request.method == "POST":

        # Extract the data
        config_file = request.json["config_file"]

        # Training
        inference_details = inference(config_file=config_file)

        return jsonify({"inference_details": inference_details})
