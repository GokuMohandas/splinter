import os
from datetime import datetime
import json
import pandas as pd
import shutil
from threading import Thread
import time
import uuid

from splinter.config import TaskConfig, DataConfig, TrainingConfig, InferenceConfig
from splinter.utils import handle_dirs
from splinter.ml.utils import set_seed_everywhere
from splinter.ml.training import train_operations
from splinter.ml.inference import infer

def get_tasks():

    # Configurations
    task_config = TaskConfig()

    # Get tasks
    tasks = [f for f in os.listdir(task_config.TASKS_DIR) \
        if os.path.isdir(os.path.join(task_config.TASKS_DIR, f))]

    return tasks

def get_data_versions(task):

    # Configurations
    task_config = TaskConfig(task=task)

    # Get data ids
    data_versions = [f for f in os.listdir(task_config.DATAS_DIR) \
        if os.path.isdir(os.path.join(task_config.DATAS_DIR, f))]

    return data_versions

def get_model_versions(task):

    # Configurations
    task_config = TaskConfig(task=task)

    # Get data ids
    model_versions = [f for f in os.listdir(task_config.MODELS_DIR) \
        if os.path.isdir(os.path.join(task_config.MODELS_DIR, f))]

    return model_versions

def create_task(task, description, tags):

    # Check is task name is already taken
    tasks = get_tasks()
    if task in tasks:
        return {"response": "Task name {0} is already in use.".format(task)}

    # Configurations
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    task_config = TaskConfig(task=task)

    # Create task dir, datas_dir and models_dir
    handle_dirs(task_config.TASKS_DIR)
    handle_dirs(task_config.TASK_DIR)
    handle_dirs(task_config.DATAS_DIR)
    handle_dirs(task_config.MODELS_DIR)

    # Data details
    task_details = {
        "task": task,
        "description": description,
        "tags": tags,
        "creation_time": timestamp
    }

    # Save data details
    with open(task_config.TASK_DETAILS_FILE, "w") as f:
        json.dump(task_details, f)

    return task_details

def get_task_details(task):

    # Configurations
    task_config = TaskConfig(task=task)

    # Get data details
    with open(task_config.TASK_DETAILS_FILE, "r") as f:
        task_details = json.load(f)

    return task_details

def post_data(config_file):

    # Read configurations
    with open(config_file, "r") as f:
        config = json.load(f)

    # Configurations
    timestamp = datetime.now()
    timestamp_string = timestamp.strftime("%Y-%m-%d %H:%M:%S")
    config["DATA_VERSION"] = "{}_{}".format(
        timestamp.strftime("%Y-%m-%d_%H:%M:%S"), uuid.uuid1())
    data_config = DataConfig(config=config)

    # Load the data
    df = pd.read_csv(config["ORIGIN"])

    # Create data dir
    handle_dirs(os.path.join(data_config.DATA_DIR))

    # Data tests (sanity and validity tests)
    pass

    # Save the data (raw data is sacrosanct, you must save it)
    df.to_csv(data_config.RAW_DATA_FILE, index=False)

    # Save config file
    with open(data_config.DATA_CONFIG_FILE, "w") as f:
        json.dump(config, f)

    # Data details
    data_details = {
        "data_version": data_config.DATA_VERSION,
        "description": config["DESCRIPTION"],
        "location": data_config.RAW_DATA_FILE,
        "task": data_config.TASK,
        "creation_time": timestamp_string,
        "size": len(df),
        "origin": config["ORIGIN"]
    }

    # Save data details
    with open(data_config.DATA_DETAILS_FILE, "w") as f:
        json.dump(data_details, f)

    return data_details

def get_data_details(task, data_version):

    # Configurations
    task_config = TaskConfig(task=task, data_version=data_version)

    # Get data details
    with open(task_config.DATA_DETAILS_FILE, "r") as f:
        data_details = json.load(f)

    return data_details

def train(config_file):

    # Read configurations
    with open(config_file, "r") as f:
        config = json.load(f)

    # Configurations
    timestamp = datetime.now()
    timestamp_string = timestamp.strftime("%Y-%m-%d %H:%M:%S")
    config["TIMESTAMP"] = timestamp_string
    config["MODEL_VERSION"] = "{}_{}".format(
        timestamp.strftime("%Y-%m-%d_%H:%M:%S"), uuid.uuid1())
    training_config = TrainingConfig(config=config)

    # Validate data_version is valid
    data_versions = get_data_versions(task=config["TASK"])
    if config["DATA_VERSION"] not in data_versions:
        raise Exception("Invalid data version.")

    # Set seed for reproducability
    set_seed_everywhere(seed=training_config.SEED, cuda=training_config.CUDA)

    # Create model storage
    handle_dirs(training_config.MODEL_DIR)

    # Training operations
    thread = Thread(target=train_operations, args=(training_config,))
    thread.start()

    # Save config file
    with open(training_config.TRAINING_CONFIG_FILE, "w") as f:
        json.dump(config, f)

    # training details
    training_details = {
        "model_version": training_config.MODEL_VERSION,
        "data_version": config["DATA_VERSION"],
        "creation_time": timestamp_string,
        "task": training_config.TASK,
        "description": config["DESCRIPTION"],
        "done_training": False,
    }

    # Add other items from config
    for key, val in config.items():
        training_details[key.lower()] = val

    # Save data details
    with open(training_config.TRAINING_DETAILS_FILE, "w") as f:
        json.dump(training_details, f)

    return training_details

def get_training_details(task, model_version):

    # Configurations
    task_config = TaskConfig(task=task, model_version=model_version)

    # Get training details
    with open(task_config.TRAINING_DETAILS_FILE, "r") as f:
        training_details = json.load(f)

    return training_details

def delete_task(task):

    # Configurations
    task_config = TaskConfig(task=task)

    # Delete the dir recursively if it exists
    shutil.rmtree(task_config.TASK_DIR)

    # Update response
    return "Successfully deleted task {0}".format(task)

def delete_data(task, data_version):

    # Configurations
    task_config = TaskConfig(task=task, data_version=data_version)

    # Delete the dir recursively if it exists
    shutil.rmtree(task_config.DATA_DIR)

    # Update response
    return "Successfully deleted data version {0}".format(data_version)

def delete_model(task, model_version):

    # Configurations
    task_config = TaskConfig(task=task, model_version=model_version)

    # Delete the dir recursively if it exists
    shutil.rmtree(task_config.MODEL_DIR)

    # Update response
    return "Successfully deleted model version {0}".format(model_version)

def inference(config_file):

    # Read configurations
    with open(config_file, "r") as f:
        config = json.load(f)

    # Configurations
    timestamp = datetime.now()
    timestamp_string = timestamp.strftime("%Y-%m-%d %H:%M:%S")
    config["TIMESTAMP"] = timestamp
    config["INFERENCE_VERSION"] = "{}_{}".format(
        timestamp.strftime("%Y-%m-%d_%H:%M:%S"), uuid.uuid1())
    inference_config = InferenceConfig(config=config)

    # training configurations needed
    with open(inference_config.TRAINING_CONFIG_FILE, "r") as f:
        training_config_file = json.load(f)
    training_config = TrainingConfig(config=training_config_file)

    # Set seed for reproducability
    set_seed_everywhere(seed=inference_config.SEED, cuda=inference_config.CUDA)

    # Create inference storage
    handle_dirs(inference_config.INFERENCES_DIR)
    handle_dirs(inference_config.INFERENCE_DIR)

    # Inference operations
    thread = Thread(target=infer, args=(training_config, inference_config,))
    thread.start()

    # Save config file
    with open(inference_config.INFERENCE_CONFIG_FILE, "w") as f:
        json.dump(config, f)

    # Inference details
    inference_details = {
        "task": training_config.TASK,
        "creation_time": timestamp_string,
        "predictions": inference_config.INFERENCE_FILE
    }

    # Update response
    return inference_details

