import os
import sys
import json
import requests

from splinter.config import BASE_DIR

if __name__ == "__main__":

    method = sys.argv[1]
    TASK = "iris"
    DATA_VERSION = "2018-07-28-18:00:36_cdd63d18-92ca-11e8-b30c-c4b301baf8bb"
    MODEL_VERSION = "2018-07-28-18:01:36_f123e180-92ca-11e8-912d-c4b301baf8bb"

    if method == "ping":
        url = "http://localhost:5000/api/"
        r = requests.get(url)
        print (r.json())

    elif method == "tasks":
        url = "http://localhost:5000/api/tasks/"
        r = requests.get(url)
        print (r.json())

    elif method == "data":
        url = "http://localhost:5000/api/data/{0}".format(TASK)
        r = requests.get(url)
        print (r.json())

    elif method == "models":
        url = "http://localhost:5000/api/models/{0}".format(TASK)
        r = requests.get(url)
        print (r.json())

    elif method == "create_task":
        data = {
            "config_file": os.path.join(BASE_DIR, "tests", "iris", "task_config.json")
        }
        url = "http://localhost:5000/api/create_task/"
        r = requests.post(url, json=data)
        print (r.json())

    elif method == "task_details":
        url = "http://localhost:5000/api/task_details/{0}/".format(TASK)
        r = requests.get(url)
        print (r.json())

    elif method == "post_data":
        data = {
            "config_file": os.path.join(BASE_DIR, "tests", "iris", "data_config.json")
        }
        url = "http://localhost:5000/api/post_data/"
        r = requests.post(url, json=data)
        print (r.json())

    elif method == "data_details":
        url = "http://localhost:5000/api/data_details/{0}/{1}/".format(TASK, DATA_VERSION)
        r = requests.get(url)
        print (r.json())

    elif method == "train_model":
        data = {
            "config_file": os.path.join(BASE_DIR, "tests", "iris", "training_config.json")
        }
        url = "http://localhost:5000/api/train_model/"
        r = requests.post(url, json=data)
        print (r.json())

    elif method == "training_details":
        url = "http://localhost:5000/api/training_details/{0}/{1}/".format(TASK, MODEL_VERSION)
        r = requests.get(url)
        print (r.json())

    elif method == "delete_task":
        url = "http://localhost:5000/api/delete_task/{0}/".format(TASK)
        r = requests.get(url)
        print (r.json())

    elif method == "delete_data":
        url = "http://localhost:5000/api/delete_data/{0}/{1}/".format(TASK, DATA_VERSION)
        r = requests.get(url)
        print (r.json())

    elif method == "delete_model":
        url = "http://localhost:5000/api/delete_model/{0}/{1}/".format(TASK, MODEL_VERSION)
        r = requests.get(url)
        print (r.json())

    elif method == "infer":
        data = {
            "config_file": os.path.join(BASE_DIR, "tests", "iris", "inference_config.json")
        }
        url = "http://localhost:5000/api/infer/"
        r = requests.post(url, json=data)
        print (r.json())




    else:
        raise Exception("Please invoke a proper API method.")

