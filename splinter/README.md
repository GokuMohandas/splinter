# Splinter

### API endpoints
* [Health check](splinter/docs/ping.md) : `GET /api/`
* [Tasks](splinter/docs/tasks.md) : `GET /api/tasks`
* [Data](splinter/docs/data.md) : `GET /api/data/:task/`
* [Models](splinter/docs/models.md) : `GET /api/models/:task/`
* [Post data](splinter/docs/post_data.md) : `POST /api/post_data/`
* [Data details](splinter/docs/data_details.md) : `GET /api/data_details/:data_version/`
* [Train](splinter/docs/train.md) : `POST /api/train/`
* [Training details](splinter/docs/training_details.md) : `GET /api/training_details/:model_version/`
* [Delete data](splinter/docs/delete_task.md) : `GET /api/model_details/:task/`
* [Delete data](splinter/docs/delete_data.md) : `GET /api/model_details/:task/:data_version/`
* [Delete model](splinter/docs/delete_model.md) : `GET /api/delete_model/:task/:model_version/`
* [Infer](splinter/docs/infer.md): `POST /api/infer/`

### Tests
```
python tests/test.py ping
python tests/test.py tasks
python tests/test.py data
python tests/test.py models
python tests/test.py post_data
python tests/test.py data_details
python tests/test.py train
python tests/test.py training_details
python tests/test.py delete_data
python tests/test.py delete_model
python tests/test.py delete_task
python tests/test.py infer
```

### ML task
This specific ML example is for the Iris flower classification task where all the independent variables are continuous and the dependent variable (species) is categorical. You should replace the ml with your own ml code for your task. Only contraints are that it needs an input and produces an output :) For our Iris example, the last thing you need is a neural network but I wanted to demonstrate the elegance and power of the PyTorch framework. You can easily change the code to do very powerful things for advanced CV/NLP tasks!

### TODO
- validate that the task name is unique
- do everything without config files (you can use prefilled parameters too)
- do everything without the comand line
- Generalizing so it doesnt matter wether it's classificaiton or regression or what people want to measure, etc.



