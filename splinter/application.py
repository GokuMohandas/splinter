import os
from flask import Flask, redirect, session, render_template, url_for

from splinter.config import cache, flask_logger, ml_logger, DevelopmentConfig, ProductionConfig
from splinter.views.api.api import _api
from splinter.views.dashboard.tasks.tasks import _tasks
from splinter.views.dashboard.data.data import _data
from splinter.views.dashboard.models.models import _models

# Define the WSGI applicationlication object
application = Flask(__name__)
#cache.init_app(application)
application.register_blueprint(_api)
application.register_blueprint(_tasks)
application.register_blueprint(_data)
application.register_blueprint(_models)
application.config.from_object(DevelopmentConfig)

@application.before_request
def make_session_permanent():
    """Make the current session permanent
    but then set the timelimit to 60 minutes
    in config.py
    """

    #session['PERMANENT'] = True
    session['PERMANENT'] = False

# 404
@application.errorhandler(404)
def page_not_found(e):
    """Redirect all nonexistent URLS.
    """
    return render_template("error.html")

# Internal error
@application.errorhandler(500)
def internal_error(error):
    return render_template("error.html")
