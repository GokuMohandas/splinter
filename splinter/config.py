import os
from datetime import datetime, timedelta
from flask_caching import Cache
import logging
import torch
import uuid

from splinter.utils import setup_logger, handle_dirs

# Base directory
BASE_DIR = os.path.dirname(__file__)

with open(os.path.join(BASE_DIR, "version.txt")) as f:
    version = f.read()

# Loggers
log_dir = os.path.join(BASE_DIR, 'logs'); handle_dirs(log_dir)
flask_logger = setup_logger(name='werkzeug', log_file=os.path.join(log_dir, 'flask.log'))
ml_logger = setup_logger(name='app', log_file=os.path.join(log_dir, 'ml.log'))

# Cache
cache = Cache(config={'CACHE_TYPE': 'simple'})

class FlaskConfig(object):
    """
    """
    # General
    SECRET_KEY = 'splinter-secret-key'
    SEND_FILE_MAX_AGE_DEFAULT = 0 # cache busting
    #PERMANENT_SESSION_LIFETIME = timedelta(minutes=60)

class DevelopmentConfig(FlaskConfig):
    """
    """
    DEBUG = True
    HOST = '0.0.0.0'
    PORT = 5000

class ProductionConfig(FlaskConfig):
    """
    """
    DEBUG = False
    HOST = '0.0.0.0'
    PORT = 5000

class TaskConfig(object):
    def __init__(self, task=None, data_version=None,
        model_version=None, inference_version=None):

        self.TASKS_DIR = os.path.join(BASE_DIR, "tasks")

        if task:
            # Task and data directories
            self.TASK = task
            self.TASK_DIR = os.path.join(self.TASKS_DIR, self.TASK)
            self.DATAS_DIR = os.path.join(self.TASK_DIR, "data")
            self.MODELS_DIR = os.path.join(self.TASK_DIR, "models")
            self.TASK_CONFIG_FILE = os.path.join(self.TASK_DIR, "task_config.json")
            self.TASK_DETAILS_FILE = os.path.join(self.TASK_DIR, "task_details.json")

        if task and data_version:
            # Data files
            self.DATA_VERSION = data_version
            self.DATA_DIR = os.path.join(self.DATAS_DIR, self.DATA_VERSION)
            self.DATA_DETAILS_FILE = os.path.join(self.DATA_DIR, "data_details.json")
            self.DATA_CONFIG_FILE = os.path.join(self.DATA_DIR, "data_config.json")
            self.RAW_DATA_FILE = os.path.join(self.DATA_DIR, "raw_data.csv")

        if task and model_version:
            # Training files
            self.MODEL_VERSION = model_version
            self.MODEL_DIR = os.path.join(self.MODELS_DIR, self.MODEL_VERSION)
            self.PROCESSED_DATA_FILE = os.path.join(self.MODEL_DIR, "processed_data.csv")
            self.VECTORIZER_FILE = os.path.join(self.MODEL_DIR, "vectorizer.json")
            self.MODEL_FILE =  os.path.join(self.MODEL_DIR, "model.pth")
            self.TRAINING_CONFIG_FILE = os.path.join(self.MODEL_DIR, "training_config.json")
            self.TRAINING_DETAILS_FILE = os.path.join(self.MODEL_DIR, "training_details.json")

            if inference_version:
                # Predictions files
                self.INFERENCE_VERSION = inference_version
                self.INFERENCES_DIR = os.path.join(self.MODEL_DIR, "predictions")
                self.INFERENCE_DIR = os.path.join(self.INFERENCES_DIR, self.INFERENCE_VERSION)
                self.INFERENCE_FILE = os.path.join(self.INFERENCE_DIR, "predictions.csv")
                self.INFERENCE_CONFIG_FILE = os.path.join(self.INFERENCE_DIR, "inference_config.json")

class DataConfig(TaskConfig):

    def __init__(self, config):

        # Task details
        self.TASK = config["task"]
        self.DATA_VERSION = config["data_version"]
        self.ORIGIN = config["origin"] # where the data is coming form
        super().__init__(task=self.TASK, data_version=self.DATA_VERSION)

class TrainingConfig(TaskConfig):

    def __init__(self, config):

        # Model details
        self.TASK = config["task"]
        self.TARGET_METRIC = config["target_metric"]
        self.DATA_VERSION = config["data_version"]
        self.NUM_CLASSES = config["num_classes"]
        self.FEATURES = config["features"]
        self.TIMESTAMP = config["timestamp"]
        self.MODEL_VERSION = config["model_version"]

        # Initialization seed
        self.SEED = config["seed"]

        # CUDA
        self.CUDA = True if config["cuda"] else False
        if not torch.cuda.is_available():
            self.CUDA = False

        # Splitting configuration
        self.TRAIN_PROPORTION = config["train_proportion"]
        self.VAL_PROPORTION = config["val_proportion"]
        self.TEST_PROPORTION = config["test_proportion"]
        self.SHUFFLE = config["shuffle"] # no shuffling for time series

        # Processing configuration
        self.DROP_COLUMNS = config["drop_columns"]

        self.VERBOSE = True if config["verbose"] else False
        self.RELOAD_FROM_FILES = True if config["reload_from_files"] else False
        self.NUM_EPOCHS = config["num_epochs"]
        self.BATCH_SIZE = config["batch_size"]
        self.NUM_HIDDEN_UNITS = config["num_hidden_units"]
        self.DROPOUT_P = config["dropout_p"]
        self.EARLY_STOPPING_CRITERIA = config["early_stopping_criteria"]
        self.LEARNING_RATE = config["learning_rate"]

        # Get all super configs
        super().__init__(task=self.TASK, data_version=self.DATA_VERSION,
            model_version=self.MODEL_VERSION)

class InferenceConfig(TaskConfig):

    def __init__(self, config):

        self.TASK = config["task"]
        self.MODEL_VERSION = config["model_version"]
        self.ORIGIN = config["origin"] # where the data is coming form
        self.TIMESTAMP = config["timestamp"]
        self.INFERENCE_VERSION = config["inference_version"]

        # Initialization seed
        self.SEED = config["seed"]

        # CUDA
        self.CUDA = True if config["cuda"] else False
        if not torch.cuda.is_available():
            self.CUDA = False

        # Get all super configs
        super().__init__(task=self.TASK, model_version=self.MODEL_VERSION,
            inference_version=self.INFERENCE_VERSION)

