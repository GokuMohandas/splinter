from setuptools import setup, find_packages, dist

# Required packages
build_requires = [package for package,version in [line.split ("==") for line in
    open("requirements.txt").read().split("\n") if line]]

# Define package version
version = open("version.txt").read().rstrip()

# Setup
setup(name="splinter", version=version, description="ML Platform",
    url="https://exposeai.com", author="Goku Mohandas",
    author_email="letxexposeai@gmail.com",
    long_description="Robust, scalable and secure platform for ML pipelines.",
    packages=find_packages(), #setup_requires = build_requires,
    #install_requires = build_requires
    )
